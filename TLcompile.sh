#!/bin/bash

FNAME=$(date +"%Y-%m-%d")

cd $FNAME
convert -delay 12 -loop 0 *.jpeg $FNAME.gif

cd ..