# README #

# rpiLights

This project was originally for a class project which had the objective of teaching us about setting up and implementing hardware 
with a device such as a raspberry pi. The original Project is from the [rpi_ws281x](https://github.com/jgarff/rpi_ws281x) library created by [Jeremy Garff](https://github.com/jgarff).
These links and setup instructions can also be found [here](https://learn.adafruit.com/neopixels-on-raspberry-pi/overview).
The original implementation was made to work for boards of many sizes. My implementation is specifically for use on a 16x16 board.
Currently this project outputs onto the board the time from the Raspberry Pi, the temperature from a specified zip code, a message given by a user, and calls a shell script to start another 
python file which aids with taking photos of the sunset which are then compiled into a gif.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
Links to adafruit sites where you can purchase the necessary hardware can be found [here](https://learn.adafruit.com/neopixels-on-raspberry-pi/wiring). This link will also show
wiring instructions for this project.


### Prerequisites

What things you need to install the software and how to install them
To start you will need a Python compiler. My current version is 2.7.13
For the html parsing for the temperature you will need beautiful soup and lxml

### Installing


For the instructions on how to install and test the libraries for the rpi_ws281x lights follow the instructions at [this link](https://learn.adafruit.com/neopixels-on-raspberry-pi/overview).

For instalation of the fswebcam package run the following command.

```
sudo apt-get install fswebcam
```

setup web server for user input. These instructions are for apache2. Others can be used as long as they can handle PHP 

```
sudo apt-get install apache2 -y
```
installation of imagemagick for gif compilation
```
sudo apt-get update
sudo apt-get install imagemagick -y
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests
to test to see if the apache server was setup correctly open a web browser on your pi and type "http://localhost/" or,
"http://127.0.0.1"(replace 127.0.0.1 with your pi's IP address) on another computer connected to the same network. 

## Authors

[Jeremy Garff](https://github.com/jgarff) who made the repository for the base of this project
See also the list of [contributors] for the rpi_ws281x repository(https://github.com/jgarff/rpi_ws281x/graphs/contributors)

[Kyle Brown](https://bitbucket.org/kjbrown503projects/) All work between the initial rpi_ws281x repository and this projects current stage was done by me  

## Acknowledgments

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth) for the README.md template
* **Stackoverflow** - the community was very helpful to me with this project 