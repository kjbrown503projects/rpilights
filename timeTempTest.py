#!/user/bin/env python

# NeoPixel library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Time, temperature, message display on 16x16 ws2810 light board
# Kyle Brown (kjbrown503@gmail.com)
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.

import time
import subprocess
from datetime import datetime
from neopixel import *
import requests
import json



# LED strip configuration:
LED_COUNT      = 256      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
#board = [] # boolean array  for spot either on or off.
lastTime=0
badConnection=False
current=False
sunset=""
sunCaped=False
flipX=False
flipY=False
apiKey=""
lastTemperature=40.00
storedDate="2017-05-03"


# Define functions which animate LEDs in various ways.


def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)

def rainbow(strip, wait_ms=20, iterations=1):
    """Draw rainbow that fades across all pixels at once."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((i+j) & 255))
        strip.show()
        time.sleep(wait_ms/1000.0)

def rainbowCycle(strip, wait_ms=20, iterations=5):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((int(i * 256 / strip.numPixels()) + j) & 255))
        strip.show()
        time.sleep(wait_ms/1000.0)

def wipe(strip, color,speed):
    """Wipe color across display a pixel at once."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        if(speed%2==1):
            strip.show()
    strip.show()

def customRainbow(strip, board, wait_ms=20, iterations=1):
    """Draw rainbow that fades across all pixels at once."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
                        if((i/16%2==0)):
                                if(board[i/16][i%16]==True):
                                        strip.setPixelColor(i, wheel((i+j) & 255))
                        else:
                                if(board[i/16][15-i%16]==True):
                                        strip.setPixelColor(i, wheel((i+j) & 255))
        strip.show()
        time.sleep(wait_ms/1000.0)


def rainbowScroll(strip, board, rainbowCount, wait_ms=30):
    """Draw rainbow that fades across all pixels at once."""
##  print (board)
    for j in range((rainbowCount*8),((rainbowCount+1)*8)):
        for i in range(strip.numPixels()):
                        if((i/16%2==0)):
                                if(board[i/16][i%16]==True):
                                        strip.setPixelColor(i, wheel((i+j) & 255))
                        else:
                                if(board[i/16][15-i%16]==True):
                                        strip.setPixelColor(i, wheel((i+j) & 255))
        strip.show()



def customSet(strip, color,board):
    """Wipe color across display a pixel at once."""
    for j in range(350):
                for i in range(strip.numPixels()):
                        if((i/16%2==0)):
                                if(board[i/16][i%16]==True):
                                        strip.setPixelColor((i), color)
                        else:
                                if(board[i/16][15-i%16]==True):
                                        strip.setPixelColor((i), color)
                strip.show()

def customScroll(strip, color,board):
    """Wipe color across display a pixel at once."""
    for j in range(8):
                for i in range(strip.numPixels()):
                        if((i/16%2==0)):
                                if(board[i/16][i%16]==True):
                                        strip.setPixelColor((i), color)
                        else:
                                if(board[i/16][15-i%16]==True):
                                        strip.setPixelColor((i), color)
                strip.show()

def pickSet(x):
    #make this intivtive
    return {
        0: [False, False, False],
        1: [False, False, True],
        2: [False, True, False],
        3: [False, True, True],
        4: [True, False, False],
        5: [True, False, True],
        6: [True, True, False],
        7: [True, True, True],
        8: [False, False],
        9: [False,True],
        10:[True,False],
        11:[True,True],
        12:[False],
        13:[True],

    }[x]

def setT(x):

        return {
      0:    [pickSet(0),
                pickSet(7),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(7),
        pickSet(0),
        pickSet(0)],

        1: [pickSet(0),
                pickSet(3),
        pickSet(2),
        pickSet(2),
        pickSet(2),
        pickSet(2),
        pickSet(2),
        pickSet(2),
        pickSet(2),
        pickSet(2),
        pickSet(2),
        pickSet(2),
        pickSet(2),
        pickSet(7),
        pickSet(0),
        pickSet(0)],

        2: [pickSet(0),
                pickSet(7),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(4),
        pickSet(4),
        pickSet(2),
        pickSet(1),
        pickSet(1),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(7),
        pickSet(0),
        pickSet(0)],
        3:
    [pickSet(0),
                pickSet(7),
        pickSet(5),
        pickSet(5),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(7),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(5),
        pickSet(5),
        pickSet(7),
        pickSet(0),
        pickSet(0)],

        4:[pickSet(0),
                pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(7),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(0),
        pickSet(0)],

        5: [pickSet(0),
                pickSet(7),
        pickSet(5),
        pickSet(1),
        pickSet(1),
        pickSet(1),
        pickSet(1),
        pickSet(2),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(5),
        pickSet(5),
        pickSet(7),
        pickSet(0),
        pickSet(0)],

        6: [pickSet(0),
                pickSet(7),
        pickSet(1),
        pickSet(1),
        pickSet(1),
        pickSet(1),
        pickSet(1),
        pickSet(7),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(7),
        pickSet(0),
        pickSet(0)],

        7: [pickSet(0),
                pickSet(7),
        pickSet(5),
        pickSet(5),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(0),
        pickSet(0)],

         8: [pickSet(0),
                pickSet(7),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(2),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(7),
        pickSet(0),
        pickSet(0)],

    9: [pickSet(0),
                pickSet(7),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(5),
        pickSet(7),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(4),
        pickSet(0),
        pickSet(0)],

        ":":  #sets array for colon rows
            [pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(10),
             pickSet(10),
             pickSet(8),
             pickSet(10),
             pickSet(10),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8)],

        "buff": # sets array for buffer between numbers
            [pickSet(12),
             pickSet(12),
             pickSet(12),
             pickSet(12),
             pickSet(12),
             pickSet(12),
             pickSet(12),
             pickSet(12),
             pickSet(12),
             pickSet(12),
             pickSet(12),
             pickSet(12),
             pickSet(12),
             pickSet(12),
         pickSet(12),
         pickSet(12)],

        "Fe": # sets array for buffer between numbers
            [pickSet(0),
             pickSet(7),
             pickSet(1),
             pickSet(3),
             pickSet(1),
             pickSet(1),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "Ce": # sets array for buffer between numbers
            [pickSet(0),
             pickSet(7),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(7),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "deg": # sets array for buffer between numbers
            [pickSet(8),
             pickSet(10),
             pickSet(10),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
             pickSet(8),
         pickSet(8),
         pickSet(8)],

        "A": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(2),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "B": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(3),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "C": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "D": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(3),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(3),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "E": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(7),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "F": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(7),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "G": # sets array for buffer between numbers
            [4,
             (pickSet(0)+(pickSet(12))),
             (pickSet(0)+(pickSet(12))),
             (pickSet(0)+(pickSet(12))),
             (pickSet(7)+(pickSet(13))),
             (pickSet(4)+(pickSet(13))),
             (pickSet(0)+(pickSet(13))),
             (pickSet(0)+(pickSet(13))),
             (pickSet(6)+(pickSet(13))),
             (pickSet(4)+(pickSet(13))),
             (pickSet(4)+(pickSet(13))),
             (pickSet(4)+(pickSet(13))),
             (pickSet(7)+(pickSet(13))),
             (pickSet(0)+(pickSet(12))),
             (pickSet(0)+(pickSet(12))),
         (pickSet(0)+(pickSet(12))),
         (pickSet(0)+(pickSet(12)))],

        "H": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "I": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "J": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "K": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(3),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "L": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "M": # sets array for buffer between numbers
            [6,
             (pickSet(0)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
             (pickSet(7)+(pickSet(6))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(0)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
         (pickSet(0)+(pickSet(0))),
         (pickSet(0)+(pickSet(0)))],

        "N": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "O": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "P": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "Q": # sets array for buffer between numbers
            [6,
             (pickSet(0)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
             (pickSet(1)+(pickSet(6))),
             (pickSet(2)+(pickSet(1))),
             (pickSet(2)+(pickSet(1))),
             (pickSet(2)+(pickSet(1))),
             (pickSet(2)+(pickSet(1))),
             (pickSet(2)+(pickSet(1))),
             (pickSet(2)+(pickSet(5))),
             (pickSet(3)+(pickSet(1))),
             (pickSet(3)+(pickSet(6))),
             (pickSet(4)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
         (pickSet(0)+(pickSet(0))),
         (pickSet(0)+(pickSet(0)))],

        "R": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(3),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "S": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(1),
             pickSet(1),
             pickSet(7),
             pickSet(4),
             pickSet(4),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "T": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "U": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "V": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(2),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "W": # sets array for buffer between numbers
            [6,
             (pickSet(0)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(7)+(pickSet(6))),
             (pickSet(0)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
         (pickSet(0)+(pickSet(0))),
         (pickSet(0)+(pickSet(0)))],

        "X": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(2),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "Y": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(4),
             pickSet(4),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "Z": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(2),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "0": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "1": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(3),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(2),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "2": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(4),
             pickSet(4),
             pickSet(2),
             pickSet(1),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "3": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(4),
             pickSet(4),
             pickSet(7),
             pickSet(4),
             pickSet(4),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "4": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "5": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(1),
             pickSet(1),
             pickSet(2),
             pickSet(4),
             pickSet(4),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "6": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(1),
             pickSet(1),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "7": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "8": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "9": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(5),
             pickSet(5),
             pickSet(7),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        ' ': # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "!": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(0),
             pickSet(1),
             pickSet(1),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "?": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(5),
             pickSet(4),
             pickSet(4),
             pickSet(2),
             pickSet(2),
             pickSet(0),
             pickSet(2),
             pickSet(2),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        ".": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(1),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "[": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "]": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(7),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(7),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "(": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(4),
             pickSet(6),
             pickSet(3),
             pickSet(1),
             pickSet(1),
             pickSet(1),
             pickSet(3),
             pickSet(6),
             pickSet(4),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        "<": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(4),
             pickSet(2),
             pickSet(1),
             pickSet(2),
             pickSet(4),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        ">": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(1),
             pickSet(2),
             pickSet(4),
             pickSet(2),
             pickSet(1),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],

        ")": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(1),
             pickSet(3),
             pickSet(6),
             pickSet(4),
             pickSet(4),
             pickSet(4),
             pickSet(6),
             pickSet(3),
             pickSet(1),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],
      
      "\'": # sets array for buffer between numbers
            [3,
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(2),
             pickSet(2),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
             pickSet(0),
         pickSet(0),
         pickSet(0)],


        "$": # sets array for buffer between numbers
            [6,
             (pickSet(0)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
             (pickSet(1)+(pickSet(0))),
             (pickSet(7)+(pickSet(6))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(1)+(pickSet(2))),
             (pickSet(7)+(pickSet(6))),
             (pickSet(5)+(pickSet(0))),
             (pickSet(5)+(pickSet(2))),
             (pickSet(7)+(pickSet(6))),
             (pickSet(1)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
             (pickSet(0)+(pickSet(0))),
         (pickSet(0)+(pickSet(0))),
         (pickSet(0)+(pickSet(0)))],
      
      
      "Hheart": # sets array for hollow heart
            [16,
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(7))+(pickSet(0))+(pickSet(7))+(pickSet(0))),
             (pickSet(12)+(pickSet(1))+(pickSet(5))+(pickSet(5))+(pickSet(5))+(pickSet(4))),
             (pickSet(12)+(pickSet(3))+(pickSet(0))+(pickSet(7))+(pickSet(0))+(pickSet(6))),
             (pickSet(12)+(pickSet(2))+(pickSet(0))+(pickSet(2))+(pickSet(0))+(pickSet(2))),
             (pickSet(12)+(pickSet(2))+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(2))),
             (pickSet(12)+(pickSet(2))+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(2))),
             (pickSet(12)+(pickSet(3))+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(6))),
             (pickSet(12)+(pickSet(1))+(pickSet(4))+(pickSet(0))+(pickSet(1))+(pickSet(4))),
             (pickSet(12)+(pickSet(0))+(pickSet(6))+(pickSet(0))+(pickSet(3))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(3))+(pickSet(0))+(pickSet(6))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(1))+(pickSet(5))+(pickSet(4))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(7))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(2))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0)))],
      
      "Arrow": # sets array for hollow heart
            [16,
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(6))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(1))+(pickSet(7))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(3))+(pickSet(7))+(pickSet(4))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(6))+(pickSet(6))+(pickSet(6))+(pickSet(0))),
             (pickSet(12)+(pickSet(1))+(pickSet(4))+(pickSet(6))+(pickSet(3))+(pickSet(0))),
             (pickSet(12)+(pickSet(3))+(pickSet(0))+(pickSet(6))+(pickSet(1))+(pickSet(4))),
             (pickSet(12)+(pickSet(2))+(pickSet(0))+(pickSet(6))+(pickSet(0))+(pickSet(4))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(6))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(6))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(6))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(6))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(6))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(6))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(6))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0)))],
      
      "Fheart": # sets array for filled in heart
            [16,
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(7))+(pickSet(0))+(pickSet(7))+(pickSet(0))),
             (pickSet(12)+(pickSet(1))+(pickSet(7))+(pickSet(5))+(pickSet(7))+(pickSet(4))),
             (pickSet(12)+(pickSet(3))+(pickSet(7))+(pickSet(7))+(pickSet(7))+(pickSet(6))),
             (pickSet(12)+(pickSet(3))+(pickSet(7))+(pickSet(7))+(pickSet(7))+(pickSet(6))),
             (pickSet(12)+(pickSet(3))+(pickSet(7))+(pickSet(7))+(pickSet(7))+(pickSet(6))),
             (pickSet(12)+(pickSet(3))+(pickSet(7))+(pickSet(7))+(pickSet(7))+(pickSet(6))),
             (pickSet(12)+(pickSet(3))+(pickSet(7))+(pickSet(7))+(pickSet(7))+(pickSet(6))),
             (pickSet(12)+(pickSet(1))+(pickSet(7))+(pickSet(7))+(pickSet(7))+(pickSet(4))),
             (pickSet(12)+(pickSet(0))+(pickSet(7))+(pickSet(7))+(pickSet(7))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(3))+(pickSet(7))+(pickSet(6))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(1))+(pickSet(7))+(pickSet(4))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(7))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(2))+(pickSet(0))+(pickSet(0))),
             (pickSet(12)+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0))+(pickSet(0)))],

    }[x]

def temSplit(temp):
    print (temp)
    nums = [0 for x in range(2)]
    nums[0]=int(temp/10)
    nums[1]=int(temp%10)
    return nums


def tempArrayPop(temp, unit):

    global badConnection
    numsA=temSplit(temp)
    rawBoolA=[]#[[True for x in range(16)] for y in range(0)]
    rawbools=[True for x in range(16)]
    # populates sets 9 for temp

    rawBoolA.append(setT('buff'))
    rawBoolA.append(setT(unit))
    rawBoolA.append(setT('buff'))
    rawBoolA.append(setT('deg'))
    rawBoolA.append(setT('buff'))
    for i in range (2):
        rawBoolA.append(setT(numsA[1-i]))
        rawBoolA.append(setT('buff'))

            #[set][row][index]
            #change k for space and colon
    for j in range (16):
        tempbools=[]
        for i in range (9):
           if(i%2==1 and i!=3):
               for k in range (3):
                   tempbools.append(rawBoolA[i][j][k])
           elif(i==3):
               for k in range (2):
                   tempbools.append(rawBoolA[i][j][k])
           else:
                   tempbools.append(rawBoolA[i][j][0])

        rawbools[j]=tempbools

    if(badConnection==True):
            rawbools[15][0]=True;
            rawbools[15][1]=True;
    return rawbools


#split by mod and int division


def tSplit(time):
    nums = [0 for x in range(4)]
    bNums = [0 for x in range(4)]
    sets =time.split(':')
    nums[0]=int(sets[0])/10
    nums[1]=int(sets[0])%10
    nums[2]=int(sets[1])/10
    nums[3]=int(sets[1])%10
    for i in range (4):
            bNums[i]=nums[3-i]
    return bNums


def tArrayPop():
    numsA=tSplit(str(datetime.now().time()))
    rawBoolA=[]#[[True for x in range(16)] for y in range(0)]
    rawbools=[True for x in range(16)]

    for i in range (4):
        rawBoolA.append(setT(numsA[i]))
        if (i%2==0):
            rawBoolA.append(setT('buff'))
        elif i==1:
            rawBoolA.append(setT(':'))
    for j in range (16):
        tempbools=[]
        for i in range (7):
           if(i%2==0):
               for k in range (3):
                   tempbools.append(rawBoolA[i][j][k])
           elif(i==3):
               for k in range (2):
                   tempbools.append(rawBoolA[i][j][k])
           else:
                   tempbools.append(rawBoolA[i][j][0])
        rawbools[j]=tempbools

    return rawbools

def sIndexInvert(lArray):
        invArray=[[True for y in range (len(lArray[0]))] for x in range(16)]
        if (flipY and flipX):
            for j in range(16):
                for i in range(len(lArray[0])):
                    invArray[j][i]=lArray[len(lArray[j])-1-i][j]
        elif (flipY):
            for j in range(16):
                for i in range(len(lArray[0])):
                    invArray[j][i]=lArray[i][len(lArray[j])-1-j]
        elif(flipX):
            for j in range(16):
                for i in range(len(lArray[0])):
                    invArray[j][i]=lArray[15-j][len(lArray[j])-1-i]
        else:
            return lArray

        return invArray 


#1 method holds words
#show pauses and scrolls; when its 5 from end it will append to end
#converts word to booleans
def sArrayPop(word):

    rawBoolA=[]#[[True for x in range(16)] for y in range(0)]
    rawbools=[rawBoolA for x in range(16)]
    length=len(word)
    print(word)

    for i in range (length):
        rawBoolA.append(setT(word[length-1-i]))
            #[set][row][index]
            #change k for space and colon
    for j in range (1,17):
        tempbools=[]
        #print('word len', len(word))
        for i in range (length):
          #  print('i',i)
          #  print ('len of letter', rawBoolA[i][0])
            for k in range (rawBoolA[i][0]):
                tempbools.append(rawBoolA[i][j][k])
          #      print('k',k)
            if(rawBoolA[i][0]!=6):
                    tempbools=tempbools + [False]
        rawbools[j-1]=rawbools[j-1] +tempbools
    return rawbools

def staticImage(sentence):
    image=sentence[1:-1]
    end=0
    lastShowing=0
    rainbowcount=0
    print(image)
    rawBoolA=[]#[[True for x in range(16)] for y in range(0)]
    rawbools=setT(image)[1:]
    return rawbools

def messageScroll(sentence, rgb):
    words=sentence.split(",")
    end=0
    lastShowing=0
    rainbowcount=0
    #rgbPop()
    for k in range(len(words)):
        lightSet=sIndexInvert(sArrayPop(words[k]))
        end= end+ len(lightSet[1])
        for l in range(16):
            for m in range(8):
                lightSet[l]=[False]+ lightSet[l]
        for j in range((len(lightSet[0]))-(len(words[k]))):
            lastShowing += 1
            wordLen=len(lightSet[1])
            if (wordLen<16):
                for l in range (16):
                    for m in range(wordLen,16):
                        lightSet[l]=lightSet[l] + [False]
            for i in range (16):
                lightSet[i]=(lightSet[i][1:]+[False])
            wipe(strip, Color(0,0,0),0)
            if(rgb[0]=="R"):
                rainbowScroll(strip,lightSet,rainbowcount)
                rainbowcount+=1
                if (rainbowcount==31):
                    rainbowcount=0
            else:
                customScroll(strip, Color(rgb[1],rgb[0],rgb[2]),lightSet)

       # print(j,'\n')
    #print(lightSetx[i])

def inputParse():
        directory='/var/www/html/colorMeta.txt'
        f=open(directory)
        data=[]
        rgb=[]
        TFarray=[]
        hasInput=True
        temp=None
        while hasInput:
            temp=f.readline()
            if(temp!=''):
                data.append(temp)
            else:
                hasInput=False
        #y=len(t)
        if(data[0][0]=='R'):
            print(data[1])

            #can use sI
            TFarray=sIndexInvert(tArrayPop())
            wipe(strip, Color(0,0,0),0)
            customRainbow(strip,TFarray)

            #can use sI
            TFarray=sIndexInvert(tempArrayPop(tempFetch(data[1]),'Fe'))
            wipe(strip, Color(0,0,0),0)
            customRainbow(strip,TFarray)

            userInput=data [2][:-2]
            if((userInput=='Message is optional') or (userInput=='')):
                print("nada")
            else:
                if(userInput.upper().count('#')==2):
                    TFarray=sIndexInvert(staticImage(userInput))
                    wipe(strip, Color(0,0,0),0)
                    customRainbow(strip,TFarray)
                else:
                    messageScroll(userInput.upper(), 'R')

        else:
                #length of color line is 9
                print(data[0])
                #grabs rgb values
                for i in range (3):
                        rgb.append((int((data[0][((2*i)+1):(-(6-(2*i)))]),16))/3)

                TFarray=sIndexInvert(tArrayPop())
                wipe(strip, Color(0,0,0),0)
                customSet(strip, Color(rgb[1],rgb[0],rgb[2]),TFarray)
                
                TFarray=sIndexInvert(tempArrayPop(tempFetch(data[1]), 'Fe'))
                
                wipe(strip, Color(0,0,0),0)
                customSet(strip, Color(rgb[1],rgb[0],rgb[2]),TFarray)

                userInput=data [2][:-2]
                if((userInput=='Message is optional') or (userInput=='')):
                        print("nada")
                else:
                    if(userInput.upper().count('#')==2):
                        TFarray=sIndexInvert(staticImage(userInput))
                        wipe(strip, Color(0,0,0),0)
                        customSet(strip, Color(rgb[1],rgb[0],rgb[2]),TFarray)
                    else:
                        messageScroll(userInput.upper(), rgb)
        f.close()

#compares current time to sunset time if equal starts sunset capturing and gif compilation
def timeCheck():
        global sunset
        global sunCaped
        global storedDate
        global current
        currentTime=str(datetime.now().time())[:5]
        currentDate=str(datetime.now().date())
        print (currentTime)
        print (sunset)
        if(currentDate!=storedDate or storedDate=="2017-05-03"):
                storedDate=currentDate
                sunCaped=False
                current=False

        if (currentTime==sunset and sunCaped==False):
                subprocess.Popen(["sh","startSunset.sh"])
                print ("capture started")
                sunCaped=True





#fetches temperature data from wunderground
def tempFetch(zipcode):
        #read from file
        
        
        global lastTemperature
        global lastTime
        global badConnection
        global sunset
        global current
        
        try:
                badConnection=False
                resp = requests.get('https://api.openweathermap.org/data/2.5/weather?zip='+zipcode[:5]+',US&units=imperial&appid='+apiKey[:-1])
                response_dict = json.loads(resp.text)
                lastTemperature=response_dict['main']['temp']
                lastTime=datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                if (current==False):
                        sunset = str(utcSet)[-8:-3]
                        print ('380a45'%sunset)
                        current=True
        except:
                badConnection=True

        print(lastTime, badConnection, lastTemperature)
        return (lastTemperature)
    
    
#sets orientation for display on run
def initalize():
    global apiKey
    global flipY
    global flipX
    selection=""
    file='moreMeta.txt'
    
    f=open(file)
    apiKey=f.readline()
    selection=f.readline()
    f.close()
    
    if(selection=='e'):
        TFarray=sIndexInvert(staticImage("#Arrow#"))
        customSet(strip, Color(5,28,35),TFarray)
        print("Select w, a, s, or d depending on which way the arrow is pointing")
        selection = raw_input("w if it is pointing up, a if it's pointing to the left, s for down, and d for right\n")
        f=open(file,"w")
        f.seek(0)
        f.write(apiKey)
        f.write(selection)
        f.close()

    if(selection.lower()=='d' or selection.lower()=='a'):
        flipY=True
    if(selection.lower()=='d' or selection.lower()=='s'):
        flipX=True
        
        

# Driver
if __name__ == '__main__':
    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
    # Intialize the library (must be called once before other functions).
    strip.begin()
    print ('Press Ctrl-C to quit.')
    initalize()
    while True:
                inputParse()

   
