#!/bin/bash

DATE=$(date +"%H:%M:%S")
FNAME=$(date +"%Y-%m-%d")

ECHO "capturing picture and putting into" $FNAME

fswebcam -r 640x480 --no-banner /home/pi/webcam/$FNAME/$DATE.jpeg